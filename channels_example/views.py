from django.shortcuts import render

# Create your views here.
from channels_example.models import Like


def index(request):
    like = Like.objects.all()
    if like.count() != 0:
        count = like.first().like_counter
    else:
        like = Like.objects.create()
        count = like.like_counter

    return render(request, template_name='channels_example/channels.html',
                  context={"count": f"{count}"})
