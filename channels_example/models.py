from django.db import models


# Create your models here.

class Like(models.Model):
    like_counter = models.IntegerField(default=1)

    def __str__(self):
        return f"Likes: {self.like_counter}"
