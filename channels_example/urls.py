from django.urls import path

from channels_example import views

urlpatterns = [
    path('', views.index),
]