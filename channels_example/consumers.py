from channels.consumer import AsyncConsumer
from channels.db import database_sync_to_async
from channels_example.models import Like


class LikeConsumer(AsyncConsumer):
    async def websocket_connect(self, event):
        await self.send({
            "type": "websocket.accept",
        })

        # register all users to the same channel
        await self.channel_layer.group_add(
            'all',
            self.channel_name
        )

        # get the current like count or create one
        count = await self.get_or_create()

        # broadcast to all connected clients
        await self.broadcast_change(count)

    async def counter_message(self, event):
        await self.send({
            "type": "websocket.send",
            "text": event['text']
        })

    async def websocket_receive(self, event):
        # if message contains like increment it
        if dict(event)['text'] == 'like':
            new_count = await self.increment_and_get()
            await self.broadcast_change(new_count)

    async def websocket_disconnect(self, event):
        pass

    async def broadcast_change(self, count: int):
        """
        Method that sends message to all connected clients
        """
        await self.channel_layer.group_send(
            'all',
            {
                "type": "counter_message",
                "text": str(count)
            }
        )

    @database_sync_to_async
    def get_or_create(self) -> int:
        """
        If like object exists in database fetch the count if
        not create new one.
        """
        like = Like.objects.all()
        if like.count() != 0:
            return like.first().like_counter
        like = Like.objects.create()
        return like.like_counter

    @database_sync_to_async
    def increment_and_get(self) -> int:
        """
        Increment the counter and return the new number.
        Returns:
             int: current number of likes
        """
        like = Like.objects.first()
        like.like_counter += 1
        like.save()
        return like.like_counter
