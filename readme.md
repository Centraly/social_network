Social network

To match the module versions used in the project use the provided requirements.txt.

For convenience website is in debug mode and I uploaded the database with asked users / groups / relationships, 
but if you want to start fresh delete .db file, run the makemigrations, migrate and init_data.py script to create asked initial data.
(create superuser account if you need one)

Remarks:

- rereading the email you sent me I'm now unsure was I supposed to use django groups, while at the creation time 
it seemed logical to create Group model since the task is social network.
- since not much was specified regarding object retrievals (get filtering on what field in aggregated collection) I just used 
common sense considering hypothetical future resource consummation
- I didn't validate all possible corner cases in the drf since I didn't think it is important to create perfect solution rather to 
showcase knowledge.
- to access channel task part one doesn't have to be logged in
