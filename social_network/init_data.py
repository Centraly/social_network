import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'social_network.settings'

import django
django.setup()

import random
from django.contrib.auth.models import User
from api.models import Friendship, Group, GroupMembership


def init_data():
    random_names = [
        "Page",
        "Sonny",
        "Jacques",
        "Hedwig",
        "Jenee",
        "Nana",
        "Corazon",
        "Christa",
        "Sunday",
        "Britta",
    ]

    groups = [
        "user",
        "reviewer",
        "admin"
    ]

    for name in random_names:
        User.objects.get_or_create(username=name, email=f"{name}@network.com")

    users = User.objects.all()

    for user in users:
        target_count = random.randint(1, 4)
        while Friendship.get_friends(user=user).count() < target_count:
            friend = random.choice(users)
            if friend == user or Friendship.is_already_friend(user, friend):
                continue
            Friendship.objects.get_or_create(user=user, friend=friend)

    for group in groups:
        if Group.objects.filter(name=group).count() == 0:
            Group.objects.get_or_create(name=group, creator=random.choice(users))

    group_objects = Group.objects.all()
    for user in users:
        GroupMembership.objects.get_or_create(user=user, group=random.choice(group_objects))


if __name__ == '__main__':
    init_data()
