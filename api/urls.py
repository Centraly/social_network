from django.urls import path, include
from rest_framework import routers

from api.views import GroupViewSet, GroupMembershipViewSet, FriendViewSet, UserViewSet

router = routers.DefaultRouter()
router.register('user', UserViewSet, base_name='user')
router.register('group', GroupViewSet, base_name='group')
router.register('group-membership', GroupMembershipViewSet, base_name='group-membership')
router.register('friendship', FriendViewSet, base_name='friend')

urlpatterns = [
    path('', include(router.urls)),
]
