# Create your views here.
from django.contrib.auth.models import User
from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response

from api.models import Group, GroupMembership, Friendship
from api.permissions import IsObjectOwner, CanChangeMembership
from api.serializers import GroupSerializer, GroupMembershipSerializer, FriendshipSerializer, UserSerializer


class UserViewSet(viewsets.ModelViewSet):
    http_method_names = ['get', 'post', 'head', 'delete']
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAdminUser,)


class GroupViewSet(viewsets.ModelViewSet):
    http_method_names = ['get', 'post', 'head', 'delete']
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = (IsObjectOwner, IsAuthenticated)

    def perform_create(self, serializer):
        serializer.save(creator=self.request.user)


class GroupMembershipViewSet(viewsets.ModelViewSet):
    http_method_names = ['get', 'post', 'head', 'delete']
    queryset = GroupMembership.objects.all()
    serializer_class = GroupMembershipSerializer
    permission_classes = (CanChangeMembership, IsAuthenticated)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class FriendViewSet(viewsets.ModelViewSet):
    http_method_names = ['get', 'post', 'head', 'delete']
    serializer_class = FriendshipSerializer
    permission_classes = (CanChangeMembership, IsAuthenticated)

    def get_queryset(self):
        return Friendship.get_friends(self.request.user)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if request.user == instance.user or \
           request.user == instance.friend:
            self.perform_destroy(instance)

        return Response(status=status.HTTP_204_NO_CONTENT)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
