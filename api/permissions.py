from rest_framework import permissions


class IsObjectOwner(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.method in permissions.SAFE_METHODS or \
               request.method in ('POST', 'DELETE')

    def has_object_permission(self, request, view, obj):
        return obj.creator == request.user


class CanChangeMembership(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.method in permissions.SAFE_METHODS or \
               request.method in ('POST', 'DELETE')

    def has_object_permission(self, request, view, obj):
        return obj.user == request.user
