from django.contrib.auth.models import User
from django.db import models


# Create your models here.
from django.db.models import Q


class Group(models.Model):
    name = models.CharField(max_length=75, null=False, blank=False, unique=True)
    created = models.DateTimeField(auto_now_add=True)
    creator = models.ForeignKey(User, on_delete=models.CASCADE, related_name='creator')
    group_membership = models.ManyToManyField(User, through='GroupMembership')

    def __str__(self):
        return self.name


class GroupMembership(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('user', 'group',)

    @staticmethod
    def get_members(group):
        return GroupMembership.objects.filter(name=group)

    def __str__(self):
        return f"Group: {self.group.name}, User: {self.user.username}"


class Friendship(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='myself')
    friend = models.ForeignKey(User, on_delete=models.CASCADE, related_name='friend')
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ('user', 'friend',)

    @staticmethod
    def get_friends(user):
        return Friendship.objects.filter(Q(user=user) | Q(friend=user))

    @staticmethod
    def is_already_friend(user, friend):
        friends = Friendship.objects.filter(Q(user=user) | Q(friend=user))
        if friend.id in [f.id for f in friends]:
            return True
        return False

    def __str__(self):
        return self.friend.username
