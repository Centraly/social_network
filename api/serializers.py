from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.fields import CurrentUserDefault

from api.models import Friendship, GroupMembership, Group


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email')


class FriendshipSerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(
        read_only=True,
    )

    def validate(self, data):
        user = self.context['request'].user
        if data['friend'] != user:
            return data
        raise serializers.ValidationError('Sadly you cannot be friend with oneself.')

    class Meta:
        model = Friendship
        fields = ('id', 'user', 'friend', 'created')
        read_only_fields = ('created',)


class GroupSerializer(serializers.ModelSerializer):
    creator = serializers.PrimaryKeyRelatedField(
        read_only=True,
    )

    class Meta:
        model = Group
        fields = ('id', 'name', 'creator', 'created')
        read_only_fields = ('created',)


class GroupMembershipSerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(
        read_only=True,
    )

    class Meta:
        model = GroupMembership
        fields = ('id', 'user', 'group', 'created')
        read_only_fields = ('created',)
