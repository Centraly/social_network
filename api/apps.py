from django.apps import AppConfig


class NetworkApiConfig(AppConfig):
    name = 'api'
