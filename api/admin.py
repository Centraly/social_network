from django.contrib import admin

# Register your models here.
from api.models import Friendship, Group, GroupMembership

# admin.site.register(Friend)

admin.site.register(Friendship)
admin.site.register(Group)
admin.site.register(GroupMembership)
